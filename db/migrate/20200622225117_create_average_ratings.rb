class CreateAverageRatings < ActiveRecord::Migration[6.0]
  def change
    create_table :average_ratings do |t|
      t.belongs_to :movie, null: false, foreign_key: true
      t.float :value

      t.timestamps
    end
  end
end
