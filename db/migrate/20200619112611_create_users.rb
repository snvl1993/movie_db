class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :email, unique: true

      t.timestamps
    end

    change_table :movies do |t|
      t.belongs_to :user, null: false, foreign_key: { to_table: :users }
    end

    change_table :ratings do |t|
      t.belongs_to :user, null: false, foreign_key: { to_table: :users }
    end
  end
end
