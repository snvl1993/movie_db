puts 'Creating users'
user1 = User.create(email: 'test@email.com', password: '12345678', password_confirmation: '12345678')
user2 = User.create(email: 'sample@email.com', password: '12345678', password_confirmation: '12345678')
user3 = User.create(email: 'random@email.com', password: '12345678', password_confirmation: '12345678')
user4 = User.create(email: 'email@test.com', password: '12345678', password_confirmation: '12345678')
user5 = User.create(email: 'email@address.com', password: '12345678', password_confirmation: '12345678')

[user1, user2, user3, user4, user5].map(&:approve!)

puts 'Creating categories'
action = Category.create(name: 'Action')
drama = Category.create(name: 'Drama')
scifi = Category.create(name: 'Science Fiction')
thriller = Category.create(name: 'Thriller')
crime = Category.create(name: 'Crime')
comedy = Category.create(name: 'Comedy')
detective = Category.create(name: 'Detective')
horror = Category.create(name: 'Horror')

puts 'Creating movies'
movies = [
  {
    attributes: {
      title: 'Interstellar',
      user: user1,
      text: 'Interstellar is a 2014 epic science fiction film directed, co-written and co-produced by Christopher Nolan. It stars Matthew McConaughey, Anne Hathaway, Jessica Chastain, Bill Irwin, Ellen Burstyn, Matt Damon, and Michael Caine. Set in a dystopian future where humanity is struggling to survive, the film follows a group of astronauts who travel through a wormhole near Saturn in search of a new home for humanity. Brothers Christopher and Jonathan Nolan wrote the screenplay, which had its origins in a script Jonathan developed in 2007. Christopher produced Interstellar with his wife, Emma Thomas, through their production company Syncopy, and with Lynda Obst through Lynda Obst Productions. Caltech theoretical physicist Kip Thorne was an executive producer, acted as scientific consultant, and wrote a tie-in book, The Science of Interstellar. Paramount Pictures, Warner Bros., and Legendary Pictures co-financed the film. Cinematographer Hoyte van Hoytema shot it on 35 mm in anamorphic format and IMAX 70 mm. Principal photography began in late 2013 and took place in Alberta (Canada), Iceland and Los Angeles. Interstellar uses extensive practical and miniature effects and the company Double Negative created additional digital effects.'
    },
    categories: [scifi, drama, thriller],
    ratings: {
      user1 => 5,
      user2 => 3,
      user3 => 5,
      user4 => 4,
      user5 => 5,
    }
  },
  {
    attributes: {
      title: 'Parasite',
      user: user1,
      text: "Parasite (Korean: 기생충; RR: Gisaengchung) is a 2019 South Korean black comedy thriller film directed by Bong Joon-ho, who also co-wrote the screenplay with Han Jin-won. It stars Song Kang-ho, Lee Sun-kyun, Cho Yeo-jeong, Choi Woo-shik, Park So-dam, Jang Hye-jin, and Lee Jung-eun and follows the members of a poor family who scheme to become employed by a wealthy family by infiltrating their household and posing as unrelated, highly qualified individuals. Parasite premiered at the 2019 Cannes Film Festival on 21 May 2019, where it became the first South Korean film to win the Palme d'Or. It was then released in South Korea by CJ Entertainment on 30 May 2019. The film received nearly unanimous critical acclaim and is considered by many critics to be the best film of 2019 and one of the best of the 2010s. It grossed over $266 million worldwide on a production budget of about $11 million, becoming the highest-grossing South Korean film. Among its numerous accolades, Parasite won a leading four awards at the 92nd Academy Awards: Best Picture, Best Director, Best Original Screenplay, and Best International Feature Film, becoming the first non-English film to win the Academy Award for Best Picture.[note 1] It was also the first South Korean film to receive Academy Award recognition, and the first film since 1955's Marty (and third film overall) to win both the Palme d'Or and the Academy Award for Best Picture. It also won the Golden Globe Award for Best Foreign Language Film and the BAFTA Award for Best Film Not in the English Language, and became the first film not in English to win the Screen Actors Guild Award for Outstanding Performance by a Cast in a Motion Picture. At the 56th Grand Bell Awards, Parasite earned a leading 11 nominations with 5 awards (the most for the show) to its name."
    },
    categories: [drama, comedy],
    ratings: {
      user1 => 5,
      user2 => 3,
      user5 => 5,
    }
  },
  {
    attributes: {
      title: '1917',
      user: user1,
      text: '1917 is a 2019 British war film directed, co-written, and produced by Sam Mendes. The film stars George MacKay and Dean-Charles Chapman, with Mark Strong, Andrew Scott, Richard Madden, Claire Duburcq, Colin Firth and Benedict Cumberbatch in supporting roles. It is based in part on an account told to Mendes by his paternal grandfather, Alfred Mendes,[7] and tells the story of two young British soldiers during the First World War who are ordered to deliver a message calling off an attack doomed to fail soon after the German retreat to the Hindenburg Line during Operation Alberich. This message is especially important to one of the soldiers, as his brother is due to take part in the attack. The project was officially announced in June 2018, with MacKay and Chapman signing on in October and the rest of the cast the following March. Filming took place from April to June 2019 in the UK, with cinematographer Roger Deakins and editor Lee Smith using long takes to have the entire film appear as two continuous shots.[8][9][10] 1917 premiered in the UK on 4 December 2019 and was released theatrically in the United States on 25 December by Universal Pictures and in the United Kingdom on 10 January 2020 by Entertainment One. The film received widespread praise and was a box office success, grossing $368 million worldwide. It received numerous accolades, including ten nominations at the 92nd Academy Awards and three wins for Best Cinematography, Best Visual Effects, and Best Sound Mixing. It also won Best Motion Picture – Drama and Best Director at the 77th Golden Globe Awards, and won a leading seven awards at the 73rd British Academy Film Awards, including Best Film and Best Direction. It also won the Producers Guild of America Award for Best Theatrical Motion Picture, and Mendes won the Directors Guild of America Award for Outstanding Directing – Feature Film. It was chosen by both the National Board of Review and the American Film Institute as one of the Top 10 Films of 2019. It was the last film to win the Academy Award for Best Sound Mixing before the award was combined with Best Sound Editing as a single award for Best Sound.'
    },
    categories: [action, drama],
    ratings: {
      user1 => 4,
      user3 => 5,
      user4 => 4,
      user5 => 5,
    }
  },
  {
    attributes: {
      title: 'Jocker',
      user: user1,
      text: "Joker is a 2019 American psychological thriller film directed and produced by Todd Phillips, who co-wrote the screenplay with Scott Silver. The film, based on DC Comics characters, stars Joaquin Phoenix as the Joker and provides a possible origin story for the character. Set in 1981, it follows Arthur Fleck, a failed stand-up comedian whose descent into insanity and nihilism inspires a violent counter-cultural revolution against the wealthy in a decaying Gotham City. Robert De Niro, Zazie Beetz, Frances Conroy, Brett Cullen, Glenn Fleshler, Bill Camp, Shea Whigham, and Marc Maron appear in supporting roles. Joker was produced by Warner Bros. Pictures, DC Films, and Joint Effort, in association with Bron Creative and Village Roadshow Pictures, and distributed by Warner Bros. Phillips conceived Joker in 2016 and wrote the script with Silver throughout 2017. The two were inspired by 1970s character studies and the films of Martin Scorsese (particularly Taxi Driver and The King of Comedy), who was initially attached to the project as a producer. The graphic novel Batman: The Killing Joke (1988) was the basis for the film's premise, but Phillips and Silver otherwise did not look to specific comics for inspiration. Phoenix became attached in February 2018 and was cast that July, while the majority of the cast signed on by August. Principal photography took place in New York City, Jersey City, and Newark, from September to December 2018. Joker is the first live-action theatrical Batman film to receive an R-rating from the Motion Picture Association of America, due to its violent and disturbing content."
    },
    categories: [crime, drama, thriller],
    ratings: {
      user1 => 5,
      user2 => 3,
      user3 => 4,
      user4 => 4,
      user5 => 5,
    }
  },
  {
    attributes: {
      title: 'Dark',
      user: user2,
      text: "Dark is a German science fiction thriller web television series co-created by Baran bo Odar and Jantje Friese.[5][6][7] Set in the fictitious town of Winden, Germany, Dark concerns the aftermath of a child's disappearance which exposes the secrets of, and hidden connections among, four estranged families as they slowly unravel a sinister time travel conspiracy which spans three generations. Throughout the series, Dark explores the existential implications of time and its effects upon human nature. Dark is the first ever German language Netflix original series; it debuted on Netflix's streaming service on 1 December 2017. The first season was received with positive reviews from critics, who made initial comparisons to another Netflix series, Stranger Things."
    },
    categories: [scifi, drama, thriller],
    ratings: {
      user1 => 4,
      user2 => 3,
      user3 => 5,
    }
  },
  {
    attributes: {
      title: 'The Room',
      user: user2,
      text: "The Room is a 2003 American romantic drama film written, produced, and directed by Tommy Wiseau, who stars in the film alongside Juliette Danielle and Greg Sestero. The film centers on a melodramatic love triangle between amiable banker Johnny (Wiseau), his deceptive fiancée Lisa (Danielle), and his conflicted best friend Mark (Sestero). A significant portion of the film is dedicated to a series of unrelated subplots, most of which involve at least one supporting character and are left unresolved due to the film's inconsistent narrative structure. The work was reportedly intended to be semi-autobiographical in nature; according to Wiseau, the title alludes to the potential of a room to be the site of both good and bad events.[4] The stage play from which the film is derived was so-called due to its events taking place entirely in a single room."
    },
    categories: [drama],
    ratings: {
      user1 => 1,
      user2 => 3,
      user3 => 2,
      user4 => 1,
      user5 => 1,
    }
  },
  {
    attributes: {
      title: 'Avengers: Endgame',
      user: user2,
      text: "Avengers: Endgame is a 2019 American superhero film based on the Marvel Comics superhero team the Avengers, produced by Marvel Studios and distributed by Walt Disney Studios Motion Pictures. It is the direct sequel to Avengers: Infinity War (2018) and the 22nd film in the Marvel Cinematic Universe (MCU). It was directed by Anthony and Joe Russo and written by Christopher Markus and Stephen McFeely, and features an ensemble cast including Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth, Scarlett Johansson, Jeremy Renner, Don Cheadle, Paul Rudd, Brie Larson, Karen Gillan, Danai Gurira, Benedict Wong, Jon Favreau, Bradley Cooper, Gwyneth Paltrow, and Josh Brolin. In the film, the surviving members of the Avengers and their allies attempt to reverse the damage caused by Thanos in Infinity War."
    },
    categories: [action, drama],
    ratings: {
      user1 => 4,
      user2 => 3,
      user3 => 2,
      user4 => 2,
      user5 => 4,
    }
  },
  {
    attributes: {
      title: 'Stranger Things',
      user: user3,
      text: "Stranger Things is an American science fiction horror web television series created by the Duffer Brothers and released on Netflix. The twins serve as showrunners and are executive producers along with Shawn Levy and Dan Cohen. The series premiered on Netflix on July 15, 2016. Set in the 1980s in the fictional town of Hawkins, Indiana, the first season focuses on the investigation into the disappearance of a young boy amid supernatural events occurring around the town, including the appearance of a girl with psychokinetic abilities. The series stars an ensemble cast including Winona Ryder, David Harbour, Finn Wolfhard, Millie Bobby Brown, Gaten Matarazzo, Caleb McLaughlin, Noah Schnapp, Sadie Sink, Natalia Dyer, Charlie Heaton, Joe Keery, Cara Buono and Dacre Montgomery."
    },
    categories: [thriller],
    ratings: {
      user1 => 4,
      user2 => 3,
      user3 => 5,
      user4 => 5,
      user5 => 4,
    }
  },
  {
    attributes: {
      title: 'Knives Out',
      user: user3,
      text: "Knives Out is a 2019 American mystery film written and directed by Rian Johnson, and produced by Johnson and Ram Bergman. A modern whodunit, the film follows a family gathering gone awry, after the patriarch's death leads a master detective to investigate. The film features an ensemble cast, including Daniel Craig, Chris Evans, Ana de Armas, Jamie Lee Curtis, Michael Shannon, Don Johnson, Toni Collette, Lakeith Stanfield, Katherine Langford, Jaeden Martell, and Christopher Plummer."
    },
    categories: [detective, thriller],
    ratings: {
      user2 => 3,
      user3 => 5,
      user4 => 5,
      user5 => 4,
    }
  },
  {
    attributes: {
      title: 'Se7en',
      user: user3,
      text: "Seven (stylized as SE7EN) is a 1995 American psychological crime thriller film directed by David Fincher and written by Andrew Kevin Walker. It stars Brad Pitt, Morgan Freeman, Gwyneth Paltrow, Kevin Spacey, R. Lee Ermey and John C. McGinley. The film tells the story of David Mills, a detective who partners with the retiring William Somerset to track down a serial killer who uses the seven deadly sins as a motif in his murders."
    },
    categories: [crime, detective, thriller],
    ratings: {
      user3 => 5,
      user4 => 5,
      user5 => 4,
    }
  },
  {
    attributes: {
      title: 'It',
      user: user3,
      text: "It, retroactively known as It Chapter One, is a 2017 American supernatural horror film based on Stephen King's 1986 novel of the same name.[5] Produced by New Line Cinema, KatzSmith Productions, Lin Pictures, Vertigo Entertainment,[6][7] and distributed by Warner Bros.[8] It is the first film in the It film series as well as being the second adaptation following Tommy Lee Wallace's 1990 miniseries.[9][10][11] The film tells the story of seven children in Derry, Maine, who are terrorized by the eponymous being, only to face their own personal demons in the process.[12][13] The film is also known as It: Part 1 – The Losers' Club"
    },
    categories: [horror, thriller],
    ratings: {
      user1 => 2,
      user4 => 5,
      user5 => 4,
    }
  },
  {
    attributes: {
      title: 'Pulp Fiction',
      user: user2,
      text: "Pulp Fiction is a 1994 American independent neo-noir crime film written and directed by Quentin Tarantino, who conceived it with Roger Avary.[4] Starring John Travolta, Samuel L. Jackson, Bruce Willis, Tim Roth, Ving Rhames, and Uma Thurman, it tells several stories of criminal Los Angeles. The title refers to the pulp magazines and hardboiled crime novels popular during the mid-20th century, known for their graphic violence and punchy dialogue."
    },
    categories: [crime, thriller],
    ratings: {
      user1 => 5,
      user2 => 5,
      user4 => 5,
      user5 => 4,
    }
  },
  {
    attributes: {
      title: 'The Hateful Eight',
      user: user2,
      text: "The Hateful Eight (sometimes marketed as The H8ful Eight) is a 2015 American western thriller film written and directed by Quentin Tarantino. It stars Samuel L. Jackson, Kurt Russell, Jennifer Jason Leigh, Walton Goggins, Demián Bichir, Tim Roth, Michael Madsen, and Bruce Dern as eight strangers who seek refuge from a blizzard in a stagecoach stopover some time after the American Civil War."
    },
    categories: [crime, thriller],
    ratings: {
      user1 => 5,
      user2 => 5,
      user4 => 4,
      user5 => 4,
    }
  }
]

movies.each do |data|
  movie = Movie.create(data[:attributes])
  movie.categories = data[:categories]
  data[:ratings].each do |user, rate|
    movie.ratings.create(rate: rate, user: user)
  end
end
