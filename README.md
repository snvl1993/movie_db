# README

To run setup the application in dev mode you need to create a `.env` file in the root folder of the project. You need to specify database connection environment variables there such as:
- `DATABASE_NAME`
- `DATABASE_USER`
- `DATABASE_PASSWORD`

To start the dev server execute the following commands:
```
bundle exec rails db:setup
bundle exec rails s
```
This will create a database and seed it with some test data.

## Architectural decisions
For authentication gem `authlogic` is used. All the necessary auth data is stored in HttpOnly cookies.

CSRF protection cookie is included to each request and CSRF header is being checked on every actionable HTTP request (except for auth requests)

The `pg_search` gem is used for the full-text movie search. It uses built in features of the PostgreSQL to provide fast and functional search without the need of setting up and maintaining other services like Elasticsearch.

A model `AverageRating` is used to decrease the load on database, because it prevents frequent `AVG` queries on ratings, which would grow quick in the real world application. Another solution would be using a materialized view for caching the averages, but it requires much more setup (which would be an overkill for the current app scale), and has some downsides 
