require 'rails_helper'

shared_examples 'authentication required' do
  context 'logged in' do
    before :each do
      authenticate(user)
    end

    it 'is not forbidden' do
      subject
      expect(response.status).not_to be(403)
    end
  end

  context 'guest' do
    it 'is forbidden' do
      subject
      expect(response.status).to be(403)
    end
  end
end

shared_examples 'authorization required' do
  context 'logged in' do
    before :each do
      authenticate(user)
    end

    it 'is not forbidden' do
      subject
      expect(response.status).not_to be(403)
    end
  end

  context 'logged in as a wrong user' do
    before :each do
      authenticate(wrong_user)
    end

    it 'is forbidden' do
      subject
      expect(response.status).to be(403)
    end
  end
end
