require 'rails_helper'

RSpec.describe "Auth", type: :request do
  describe 'sign_in' do
    let!(:user) { create(:user) }
    let!(:inactive_user) { create(:user, active: false) }

    it 'signs in to the user account' do
      params = { email: user.email, password: user.password }
      post '/api/v1/auth/sign_in', params: params

      expect(response.status).to be(200)
      expect(json_body['user']['email']).to eq(user.email)
    end

    it 'renders error if password is wrong' do
      params = { email: user.email, password: 'invalid' }
      post '/api/v1/auth/sign_in', params: params

      expect(response.status).to be(404)
      expect(json_body['errors']['base']).to include('You have entered wrong email or password')
    end

    it 'renders error if email is wrong' do
      params = { email: 'invalid@email.com', password: user.password }
      post '/api/v1/auth/sign_in', params: params

      expect(response.status).to be(404)
      expect(json_body['errors']['base']).to include('You have entered wrong email or password')
    end

    it 'renders error if user is inactive' do
      params = { email: inactive_user.email, password: inactive_user.password }
      post '/api/v1/auth/sign_in', params: params

      expect(response.status).to be(422)
      expect(json_body['errors']['base']).to include('Your account is not active')
    end
    end

  describe 'sign_up' do
    let!(:user) { create(:user) }

    it 'creates a new user account' do
      params = { email: 'new@user.com', password: 'password', password_confirmation: 'password' }
      post '/api/v1/auth/sign_up', params: params

      expect(response.status).to be(200)
      expect(json_body['user']['email']).to eq('new@user.com')
    end

    it 'renders error if password confirmation is wrong' do
      params = { email: user.email, password: 'password', password_confirmation: 'invalid' }
      post '/api/v1/auth/sign_up', params: params

      expect(response.status).to be(422)
      expect(json_body['errors']['password_confirmation']).to include("doesn't match password")
    end

    it 'renders error if user exists' do
      params = { email: user.email, password: user.password, password_confirmation: user.password }
      post '/api/v1/auth/sign_up', params: params

      expect(response.status).to be(422)
      expect(json_body['errors']['email']).to include('has already been taken')
    end
  end
end
