require 'rails_helper'
require_relative './shared_examples/authentication'

RSpec.describe "Movies", type: :request do
  let!(:user) { create(:user) }
  let!(:wrong_user) { create(:user) }
  let!(:movie1) { create(:movie, user: user) }

  describe 'index' do
    let!(:movie2) { create(:movie) }
    let!(:movie3) { create(:movie) }

    let!(:category1) { create(:category) }
    let!(:category2) { create(:category) }

    let!(:movie_category1) { create(:movie_category, movie: movie1, category: category1) }
    let!(:movie_category2) { create(:movie_category, movie: movie2, category: category2) }

    let!(:rating1) { create(:rating, rate: 5, movie: movie1) }
    let!(:rating2) { create(:rating, rate: 4, movie: movie2) }
    let!(:rating3) { create(:rating, rate: 3, movie: movie3) }

    it 'returns all movies' do
      get '/api/v1/movies'
      ids = json_body['movies'].map { |m| m['id'] }
      expect(response.status).to be(200)
      expect(ids).to match_array([movie1.id, movie2.id, movie3.id])
    end

    it 'returns movies filtered by category' do
      get "/api/v1/movies?categories[]=#{category1.id}"
      ids = json_body['movies'].map { |m| m['id'] }
      expect(response.status).to be(200)
      expect(ids).to match_array([movie1.id])
    end

    it 'returns movies filtered by multiple categories' do
      get "/api/v1/movies?categories[]=#{category1.id}&categories[]=#{category2.id}"
      ids = json_body['movies'].map { |m| m['id'] }
      expect(response.status).to be(200)
      expect(ids).to match_array([movie1.id, movie2.id])
    end

    it 'returns movies filtered by rating' do
      get "/api/v1/movies?ratings[]=#{5}"
      ids = json_body['movies'].map { |m| m['id'] }
      expect(response.status).to be(200)
      expect(ids).to match_array([movie1.id])
    end

    it 'returns movies filtered by multiple ratings' do
      get "/api/v1/movies?ratings[]=#{5}&ratings[]=#{4}"
      ids = json_body['movies'].map { |m| m['id'] }
      expect(response.status).to be(200)
      expect(ids).to match_array([movie1.id, movie2.id])
    end

    it 'returns movies filtered by categories and ratings' do
      get "/api/v1/movies?ratings[]=#{5}&categories[]=#{category1.id}&categories[]=#{category2.id}"
      ids = json_body['movies'].map { |m| m['id'] }
      expect(response.status).to be(200)
      expect(ids).to match_array([movie1.id])
    end

    it 'returns error if category does not exist' do
      get "/api/v1/movies?categories[]=0"
      expect(response.status).to be(422)
      expect(json_body['errors']['categories']).not_to be_nil
    end

    it 'returns error if ratings is out of range' do
      get "/api/v1/movies?ratings[]=0"
      expect(response.status).to be(422)
      expect(json_body['errors']['ratings']).not_to be_nil
    end
  end

  describe 'show' do
    it 'returns movie' do
      get "/api/v1/movies/#{movie1.id}"
      expect(response.status).to be(200)
      expect(json_body['movie']['id']).to be(movie1.id)
    end
  end

  describe 'rate' do
    let(:endpoint) { "/api/v1/movies/#{movie1.id}/rate" }
    subject { post endpoint, params: { rate: 5 } }

    include_examples 'authentication required'
  end

  describe 'create' do
    let!(:category1) { create(:category) }
    let!(:category2) { create(:category) }

    describe 'authentication required' do
      let(:endpoint) { '/api/v1/movies' }
      subject { post endpoint, params: { title: 'Movie' } }

      include_examples 'authentication required'
    end

    it 'returns error if category does not exist' do
      authenticate(user)
      post "/api/v1/movies", params: { title: 'Movie', categories: [0] }
      expect(response.status).to be(422)
      expect(json_body['errors']['categories']).not_to be_nil
    end

    it 'creates movie and its categories' do
      authenticate(user)

      params = { title: 'Movie', categories: [category1.id, category2.id] }
      expect do
        post "/api/v1/movies", params: params
      end.to change { Movie.count }.by(1)

      created_movie = Movie.last
      expect(response.status).to be(200)
      expect(json_body['movie']['id']).to be(created_movie.id)
      expect(created_movie.categories).to match_array([category1, category2])
    end
  end

  describe 'update' do
    let(:endpoint) { "/api/v1/movies/#{movie1.id}" }
    subject { put endpoint, params: { rate: 5 } }

    include_examples 'authorization required'
  end

  describe 'delete' do
    let(:endpoint) { "/api/v1/movies/#{movie1.id}" }
    subject { delete endpoint, params: { rate: 5 } }

    include_examples 'authorization required'
  end
end
