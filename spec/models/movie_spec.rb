require 'rails_helper'

RSpec.describe Movie, type: :model do
  describe 'create' do
    let!(:user) { create(:user) }
    let!(:movie) { create(:movie) }

    context 'invalid attributes' do
      it 'requires title' do
        record = Movie.create(user: user)
        expect(record.valid?).to be_falsey
        expect(record.errors[:title]).to include("can't be blank")
      end

      it 'requires title uniqueness' do
        record = Movie.create(user: user, title: movie.title)
        expect(record.valid?).to be_falsey
        expect(record.errors[:title]).to include('has already been taken')
      end

      it 'requires user' do
        record = Movie.create(title: 'Movie new')
        expect(record.valid?).to be_falsey
        expect(record.errors[:user]).to include('must exist')
      end
    end

    context 'valid attributes' do
      it 'creates' do
        record = Movie.create(user: user, title: 'Movie new')
        expect(record.new_record?).to be_falsey
        expect(record.valid?).to be_truthy
      end
    end
  end

  describe 'by_rate' do
    let!(:movie1) { create(:movie) }
    let!(:movie2) { create(:movie) }

    let!(:rating1) { create(:rating, rate: 5, movie: movie1) }
    let!(:rating2) { create(:rating, rate: 4, movie: movie1) }
    let!(:rating3) { create(:rating, rate: 5, movie: movie2) }

    it 'founds movies by average rating' do
      expect(Movie.by_rate(4)).to match_array([movie1])
      expect(Movie.by_rate(5)).to match_array([movie2])
    end

    it 'founds nothing when there is no appropriate movie' do
      expect(Movie.by_rate(1)).to be_empty
    end
  end

  describe 'by_category' do
    let!(:movie1) { create(:movie) }
    let!(:movie2) { create(:movie) }
    let!(:movie3) { create(:movie) }

    let!(:category1) { create(:category) }
    let!(:category2) { create(:category) }
    let!(:category3) { create(:category) }
    let!(:category4) { create(:category) }

    let!(:movie_category1) { create(:movie_category, category: category1, movie: movie1) }
    let!(:movie_category2) { create(:movie_category, category: category2, movie: movie1) }
    let!(:movie_category3) { create(:movie_category, category: category2, movie: movie2) }
    let!(:movie_category4) { create(:movie_category, category: category3, movie: movie3) }

    it 'founds movies by category' do
      expect(Movie.by_category(category1.id)).to match_array([movie1])
      expect(Movie.by_category(category2.id)).to match_array([movie1, movie2])
    end

    it 'founds movies by multiple categories' do
      expect(Movie.by_category([category1.id, category3.id])).to match_array([movie1, movie3])
    end

    it 'founds nothing when there is no appropriate movie' do
      expect(Movie.by_category(category4.id)).to be_empty
    end
  end
end
