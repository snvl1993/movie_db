require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'create' do
    let(:email) { 'test@email.com' }
    let!(:user) { create(:user) }

    context 'invalid attributes' do
      it 'requires email' do
        record = User.create(
          password: '12345678',
          password_confirmation: '12345678'
        )

        expect(record.valid?).to be_falsey
        expect(record.errors[:email]).to include('should be a valid email address')
      end

      it 'requires email to be uniq' do
        record = User.create(
          email: user.email,
          password: '12345678',
          password_confirmation: '12345678'
        )

        expect(record.valid?).to be_falsey
        expect(record.errors[:email]).to include('has already been taken')
      end

      it 'requires email to be valid' do
        record = User.create(
          email: 'invalid',
          password: '12345678',
          password_confirmation: '12345678'
        )

        expect(record.valid?).to be_falsey
        expect(record.errors[:email]).to include('should be a valid email address')
      end

      it 'requires password' do
        record = User.create(
          email: email,
          password_confirmation: '12345678'
        )

        expect(record.valid?).to be_falsey
        expect(record.errors[:password]).to include('is too short (minimum is 8 characters)')
      end

      it 'requires password to be valid' do
        record = User.create(
          email: email,
          password: '1234',
          password_confirmation: '1234'
        )

        expect(record.valid?).to be_falsey
        expect(record.errors[:password]).to include('is too short (minimum is 8 characters)')
      end

      it 'requires password to be confirmed' do
        record = User.create(
          email: email,
          password: '12345678',
          password_confirmation: '87654321'
        )

        expect(record.valid?).to be_falsey
        expect(record.errors[:password_confirmation]).to include("doesn't match password")
      end
    end

    context 'valid attributes' do
      it 'creates' do
        record = User.create(
          email: email,
          password: '12345678',
          password_confirmation: '12345678'
        )

        expect(record.new_record?).to be_falsey
        expect(record.valid?).to be_truthy
      end
    end
  end
end
