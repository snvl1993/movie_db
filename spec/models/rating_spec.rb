require 'rails_helper'

RSpec.describe Rating, type: :model do
  describe 'create' do
    let!(:user) { create(:user) }
    let!(:movie) { create(:movie) }
    let!(:rating) { create(:rating) }

    context 'invalid attributes' do
      it 'requires movie' do
        record = Rating.create(user: user, rate: 5)
        expect(record.valid?).to be_falsey
        expect(record.errors[:movie]).to include("must exist")
      end

      it 'requires rate' do
        record = Rating.create(user: user, movie: movie)
        expect(record.valid?).to be_falsey
        expect(record.errors[:rate]).to include("can't be blank")
      end

      it 'requires rate to be in range' do
        record = Rating.create(user: user, movie: movie, rate: 0)
        expect(record.valid?).to be_falsey
        expect(record.errors[:rate]).to include('should be from 1 to 5')

        record = Rating.create(user: user, movie: movie, rate: 6)
        expect(record.valid?).to be_falsey
        expect(record.errors[:rate]).to include('should be from 1 to 5')
      end

      it 'requires user' do
        record = Rating.create(movie: movie, rate: 5)
        expect(record.valid?).to be_falsey
        expect(record.errors[:user]).to include('must exist')
      end

      it 'requires rating to have uniq user and movie combination' do
        record = Rating.create(user: rating.user, movie: rating.movie, rate: 5)
        expect(record.valid?).to be_falsey
        expect(record.errors[:movie_id]).to include('user can only rate the movie once')
      end
    end

    context 'valid attributes' do
      it 'creates' do
        record = Rating.create(user: user, movie: movie, rate: 5)
        expect(record.new_record?).to be_falsey
        expect(record.valid?).to be_truthy
      end
    end
  end
end
