require 'rails_helper'

RSpec.describe MovieCategory, type: :model do
  describe 'create' do
    let!(:category1) { create(:category) }
    let!(:category2) { create(:category) }
    let!(:movie) { create(:movie) }
    let!(:movie_category) { create(:movie_category, movie: movie, category: category1) }

    context 'invalid attributes' do
      it 'requires movie and category combination to be uniq' do
        record = MovieCategory.create(movie: movie, category: category1)
        expect(record.valid?).to be_falsey
        expect(record.errors[:movie_id]).to include('should not have the same category twice')
      end
    end

    context 'valid attributes' do
      it 'creates' do
        record = MovieCategory.create(movie: movie, category: category2)
        expect(record.new_record?).to be_falsey
        expect(record.valid?).to be_truthy
      end
    end
  end
end
