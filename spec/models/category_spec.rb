require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'create' do
    let!(:category) { create(:category) }

    context 'invalid attributes' do
      it 'requires name' do
        record = Category.create
        expect(record.valid?).to be_falsey
        expect(record.errors[:name]).to include("can't be blank")
      end

      it 'requires name uniqueness' do
        record = Category.create(name: category.name)
        expect(record.valid?).to be_falsey
        expect(record.errors[:name]).to include('has already been taken')
      end
    end

    context 'valid attributes' do
      it 'creates' do
        record = Category.create(name: 'Category new')
        expect(record.new_record?).to be_falsey
        expect(record.valid?).to be_truthy
      end
    end
  end
end
