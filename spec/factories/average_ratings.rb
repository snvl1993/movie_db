FactoryBot.define do
  factory :average_rating do
    movie { nil }
    value { 1.5 }
  end
end
