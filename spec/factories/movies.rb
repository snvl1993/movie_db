FactoryBot.define do
  factory :movie do
    sequence(:title) { |n| "Movie #{n}" }
    sequence(:text) { |n| "Movie #{n} description" }

    association :user
  end
end
