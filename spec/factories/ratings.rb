FactoryBot.define do
  factory :rating do
    rate { 1 }

    association :movie
    association :user
  end
end
