require 'rails_helper'

RSpec.describe Movies::Filter do
  subject { Movies::Filter }

  let!(:category) { create(:category) }

  context 'invalid params' do
    it 'raises error if category is invalid' do
      id = category.id + 1
      expect { subject.call(categories: [id]) }.to raise_error(ApplicationService::ValidationError)
    end

    it 'raises error if rate is invalid' do
      expect { subject.call(ratings: [0]) }.to raise_error(ApplicationService::ValidationError)
      expect { subject.call(ratings: [6]) }.to raise_error(ApplicationService::ValidationError)
    end
  end

  context 'valid params' do
    let!(:movie1) { create(:movie, title: 'Test Movie Title', text: 'Text for testing') }
    let!(:movie2) { create(:movie) }
    let!(:movie3) { create(:movie) }

    let!(:rating1) { create(:rating, rate: 5, movie: movie1) }
    let!(:rating2) { create(:rating, rate: 4, movie: movie1) }
    let!(:rating3) { create(:rating, rate: 5, movie: movie2) }

    let!(:movie_category1) { create(:movie_category, category: category, movie: movie1) }
    let!(:movie_category2) { create(:movie_category, category: category, movie: movie3) }

    context 'only by category' do
      it 'filters by category' do
        service = subject.call(categories: [category.id])
        expect(service.result).to match_array([movie1, movie3])
      end
    end

    context 'only by rating' do
      it 'filters by average rating' do
        service = subject.call(ratings: [5])
        expect(service.result).to match_array([movie2])
      end

      it 'filters by multiple ratings' do
        service = subject.call(ratings: [4, 5])
        expect(service.result).to match_array([movie1, movie2])
      end
    end

    context 'only by search' do
      it 'filters by search' do
        service = subject.call(search: 'Test')
        expect(service.result).to match_array([movie1])

        service = subject.call(search: 'Text testing')
        expect(service.result).to match_array([movie1])
      end

      it 'founds nothing when search does not match any record' do
        service = subject.call(search: 'Random string')
        expect(service.result).to match_array([])
      end
    end

    context 'by rating, category and search' do
      it 'filters' do
        service = subject.call(ratings: [4, 5], categories: [category.id], search: 'test')
        expect(service.result).to match_array([movie1])
      end
    end
  end
end
