require 'rails_helper'

RSpec.describe Movies::Facets do
  subject { Movies::Facets }

  let!(:movie1) { create(:movie) }
  let!(:movie2) { create(:movie) }
  let!(:movie3) { create(:movie) }

  let!(:category1) { create(:category) }
  let!(:category2) { create(:category) }

  let!(:movie_category1) { create(:movie_category, category: category1, movie: movie1) }
  let!(:movie_category2) { create(:movie_category, category: category2, movie: movie2) }
  let!(:movie_category3) { create(:movie_category, category: category2, movie: movie3) }

  let!(:ratings1) { create(:rating, movie: movie1, rate: 5) }
  let!(:ratings2) { create(:rating, movie: movie1, rate: 2) }
  let!(:ratings3) { create(:rating, movie: movie2, rate: 1) }
  let!(:ratings4) { create(:rating, movie: movie3, rate: 5) }

  describe 'call' do
    it 'returns correct counts for categories' do
      service = subject.call

      expect(service.result[:counts][:categories]).to match_array([
        { value: category1.id, count: 1 },
        { value: category2.id, count: 2 }
      ])
    end

    it 'returns correct options for categories' do
      service = subject.call

      expect(service.result[:options][:categories]).to match_array([
        { value: category1.id, label: category1.name },
        { value: category2.id, label: category2.name }
      ])
    end

    it 'returns correct counts for ratings' do
      service = subject.call

      expect(service.result[:counts][:ratings]).to match_array([
        { value: 1, count: 1 },
        { value: 2, count: 0 },
        { value: 3, count: 1 },
        { value: 4, count: 0 },
        { value: 5, count: 1 }
      ])
    end
  end
end
