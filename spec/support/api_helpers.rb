module ApiHelpers
  def json_body
    JSON.parse(response.body)
  end

  def authenticate(user)
    cookies['user_credentials'] = "#{user.persistence_token}::#{user.send(user.class.primary_key)}"
  end

  def set_csrf_protection_status(status, example)
    tmp = Api::V1::ApplicationController.allow_forgery_protection
    Api::V1::ApplicationController.allow_forgery_protection = status
    example.run
    Api::V1::ApplicationController.allow_forgery_protection = tmp
  end
end
