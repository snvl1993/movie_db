require 'rails_helper'

RSpec.describe Api::V1::ApplicationController, type: :controller do
  controller(Api::V1::ApplicationController) do
    def index; end

    def create; end
  end

  around :each do |example|
    set_csrf_protection_status(true, example)
  end

  describe 'CSRF' do
    shared_examples 'X-CSRF-TOKEN' do
      it 'fails when X-CSRF-TOKEN is missing' do
        subject

        expect(response.status).to be(400)
        expect(json_body['errors']['base']).to include('Invalid CSRF token')
      end

      it 'succeeds when X-CSRF-TOKEN is present' do
        # Get valid CSRF token
        get :index
        token = response.cookies['CSRF-TOKEN']
        request.headers['X-CSRF-TOKEN'] = token

        subject
        expect(response.status).to be(204)
      end
    end

    describe 'get' do
      it 'sets CSRF-TOKEN cookie' do
        get :index
        expect(response.cookies).to include('CSRF-TOKEN')
      end
    end

    describe 'post' do
      subject { post :create }
      include_examples 'X-CSRF-TOKEN'
    end

    describe 'put' do
      subject { put :create }
      include_examples 'X-CSRF-TOKEN'
    end

    describe 'patch' do
      subject { patch :create }
      include_examples 'X-CSRF-TOKEN'
    end

    describe 'delete' do
      subject { delete :create }
      include_examples 'X-CSRF-TOKEN'
    end
  end
end
