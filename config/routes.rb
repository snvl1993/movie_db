Rails.application.routes.draw do
  get 'test/create'
  namespace :api do
    namespace :v1 do
      namespace :auth do
        post :sign_in
        post :sign_up
        delete :sign_out
      end

      resources :movies do
        member do
          post :rate
        end
      end

      resources :categories, only: :index
      resource :user, only: :show
      resource :facets, only: :show
    end
  end
end
