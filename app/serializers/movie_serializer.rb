class MovieSerializer < ApplicationSerializer
  attributes :id, :title, :text, :average_rating, :user_id, :rate

  has_many :categories

  def average_rating
    object.average_rating&.value&.round(1)
  end

  def rate
    return nil unless current_user
    current_user.ratings.find_by_movie_id(object.id)&.rate
  end
end
