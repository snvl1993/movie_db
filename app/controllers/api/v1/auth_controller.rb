module Api
  module V1
    class AuthController < ApplicationController
      skip_forgery_protection except: :sign_out

      def sign_up
        @user = User.new(sign_up_params)
        @user.approve

        if @user.save
          render json: @user
        else
          render_errors status: 422, errors: @user.errors
        end
      end

      def sign_in
        @user_session = UserSession.new(sign_in_params.to_h)
        if @user_session.save
          render json: @user_session.user
        elsif @user_session.errors[:password].present? || @user_session.errors[:email].present?
          render_errors status: 404, errors: { base: ['You have entered wrong email or password'] }
        else
          render_errors status: 422, errors: @user_session.errors
        end
      end

      def sign_out
        current_user_session.destroy
        render json: { success: true }
      end

      private

      def sign_up_params
        params.permit(:email, :password, :password_confirmation, :remember_me)
      end

      def sign_in_params
        params.permit(:email, :password, :remember_me)
      end
    end
  end
end
