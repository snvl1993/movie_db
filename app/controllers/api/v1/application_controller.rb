module Api
  module V1
    class ApplicationController < ActionController::API
      include ActionController::Cookies
      include ActionController::RequestForgeryProtection

      AccessDenied = Class.new(StandardError)

      before_action :set_csrf_cookie!

      protect_from_forgery with: :exception

      rescue_from ActionController::InvalidAuthenticityToken, with: :render_invalid_csrf_token
      rescue_from ApplicationService::ValidationError, with: :render_service_errors
      rescue_from ApplicationService::ExecutionError, with: :render_service_errors
      rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
      rescue_from AccessDenied, with: :render_access_denied

      private

      def set_csrf_cookie!
        cookies['CSRF-TOKEN'] = form_authenticity_token
      end

      def current_user_session
        return @current_user_session if defined?(@current_user_session)
        @current_user_session = UserSession.find
      end

      def current_user
        return @current_user if defined?(@current_user)
        @current_user = current_user_session&.user
      end

      def authenticate!
        unless current_user
          errors = { base: 'You should be authenticated to call this action' }
          render_errors status: 403, errors: errors
        end
      end

      def render_errors(status:, errors:)
        render status: status, json: { errors: errors }
      end

      def render_invalid_csrf_token
        render_errors status: 400, errors: { base: ['Invalid CSRF token'] }
      end

      def render_not_found(e)
        render_errors status: 404, errors: { base: e.message }
      end

      def render_access_denied
        render_errors status: 403, errors: { base: ['Access Denied'] }
      end

      def render_service_errors(e)
        render_errors status: 422, errors: e.errors
      end

      def paginate(collection)
        page = params[:page].blank? ? 1 : params[:page]
        collection.page(page)
      end

      def pagination_meta(collection)
        paginated = paginate(collection)

        {
          total_count: paginated.total_pages,
          has_more: !paginated.last_page? && !paginated.out_of_range?,
          current_page: paginated.current_page
        }
      end
    end
  end
end
