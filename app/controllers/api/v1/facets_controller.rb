module Api
  module V1
    class FacetsController < ApplicationController
      def show
        service = Movies::Facets.call(filter_params)
        render json: service.result
      end

      private

      def filter_params
        {
          ratings: params[:ratings],
          categories: params[:categories]
        }
      end
    end
  end
end
