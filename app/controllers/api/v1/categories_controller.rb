module Api
  module V1
    class CategoriesController < ApplicationController
      def index
        records = Category.all
        render json: records
      end
    end
  end
end
