module Api
  module V1
    class MoviesController < ApplicationController
      before_action :authenticate!, except: [:show, :index]
      before_action :set_record, only: [:show, :update, :destroy]
      before_action :validate_permissions!, only: [:update, :destroy]

      def index
        records = Movie.filter_by(index_params).includes(:categories)

        render json: paginate(records),
          meta: pagination_meta(records),
          each_serializer: MovieSerializer
      end

      def show
        render json: @record
      end

      def create
        service = Movies::Create.call(record_params)
        render json: service.result
      end

      def update
        service = Movies::Update.call(record_params)
        render json: service.result
      end

      def destroy
        @record.destroy
        render json: @record
      end

      def rate
        service = Movies::Rate.call(rate_params)
        render json: service.result
      end

      private

      def index_params
        {
          ratings: params[:ratings],
          categories: params[:categories],
          search: params[:search]
        }
      end

      def record_params
        {
          id: params[:id],
          title: params[:title],
          text: params[:text],
          categories: params[:categories],
          user: current_user
        }
      end

      def rate_params
        {
          movie_id: params[:id],
          rate: params[:rate],
          user: current_user
        }
      end

      def set_record
        @record = Movie.find(params[:id])
      end

      def validate_permissions!
        raise AccessDenied if @record.user_id != current_user.id
      end
    end
  end
end
