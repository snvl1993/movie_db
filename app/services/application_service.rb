class ApplicationService
  attr_reader :result

  class ValidationError < StandardError
    attr_reader :errors

    def initialize(errors)
      @errors = errors
    end
  end

  class ExecutionError < StandardError
    attr_reader :errors

    def initialize(errors)
      @errors = errors
    end
  end

  class Errors < HashWithIndifferentAccess
    def []=(key, value)
      super(key, Array.wrap(value))
    end
  end

  def self.call(*args, &block)
    new(*args, &block).call
  end

  def initialize(*args, &block) end

  def call
    validate!

    # Raise error if service validation failed
    raise ValidationError.new(errors) if errors.present?

    @result = execute

    # Raise error if error happened during the service execution
    raise ExecutionError.new(errors) if errors.present?

    self
  end

  def errors
    @errors ||= Errors.new { |hash, key| hash[key] = [] }
  end

  private

  def validate!
  end

  def merge_errors(errs)
    errs.each do |key, value|
      if errors[key]
        errors[key].concat(Array.wrap(value))
      else
        errors[key] = value
      end
    end
  end
end