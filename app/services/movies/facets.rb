module Movies
  class Facets < ApplicationService
    def initialize(ratings: [], categories: [])
      @ratings = Array.wrap(ratings).map(&:to_i)
      @categories = Array.wrap(categories).map(&:to_i)
    end

    private

    attr_reader :ratings, :categories

    def execute
      {
        counts: counts,
        options: options
      }
    end

    def options
      { categories: options_categories }
    end

    def counts
      {
        categories: counts_categories,
        ratings: counts_ratings
      }
    end

    def options_categories
      Category.pluck(:id, :name)
        .map { |data| { value: data[0], label: data[1] } }
    end

    def counts_categories
      subquery = filter_subquery(ratings: ratings)
      Category.joins(:movie_categories)
        .group('categories.id')
        .where(movie_categories: { movie_id: subquery })
        .pluck('categories.id', 'COUNT(movie_categories.id)')
        .map { |data| build_option_count(*data) }
    end

    def counts_ratings
      subquery = filter_subquery(categories: categories)
      (1..5).map do |value|
        count = Movie.where(id: Movie.by_rate(value))
          .where(id: subquery)
          .count

        build_option_count(value, count)
      end
    end

    def build_option_count(value, count)
      { value: value, count: count }
    end

    def filter_subquery(params)
      Movie.filter_by(params).select(:id)
    end
  end
end