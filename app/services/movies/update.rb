module Movies
  class Update < ApplicationService
    include ::ValidateCategories

    def initialize(params)
      params = HashWithIndifferentAccess.new(params)

      @categories = Array.wrap(params[:categories]).map(&:to_i)
      @title = params[:title]
      @text = params[:text]
      @user = params[:user]
      @id = params[:id]
    end

    private

    attr_reader :id, :title, :text, :user, :categories

    def execute
      @result = false
      ActiveRecord::Base.transaction do
        movie = Movie.find(id)
        movie.assign_attributes(title: title, text: text)
        movie.categories = Category.where(id: categories).all

        if movie.save
          @result = movie
        else
          error!(movie.errors)
        end
      end
    end

    def validate!
      validate_categories!
    end

    def error!(errors)
      merge_errors(errors)
      raise ActiveRecord::Rollback
    end
  end
end
