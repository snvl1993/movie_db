module Movies
  class Create < ApplicationService
    include ::ValidateCategories

    def initialize(params)
      params = HashWithIndifferentAccess.new(params)

      @categories = Array.wrap(params[:categories]).map(&:to_i)
      @title = params[:title]
      @text = params[:text]
      @user = params[:user]
    end

    private

    attr_reader :title, :text, :user, :categories

    def execute
      @result = false
      ActiveRecord::Base.transaction do
        movie = Movie.new(title: title, text: text, user: user)
        movie.categories = Category.where(id: categories).all

        if movie.save
          @result = movie
        else
          error!(movie.errors)
        end
      end
    end

    def validate!
      validate_categories!
    end

    def error!(errors)
      merge_errors(errors)
      raise ActiveRecord::Rollback
    end
  end
end
