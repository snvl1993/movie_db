module Movies
  class Rate < ApplicationService
    def initialize(params)
      params = HashWithIndifferentAccess.new(params)

      @movie_id = params[:movie_id]
      @rate = params[:rate]
      @user = params[:user]
    end

    private

    attr_reader :movie_id, :rate, :user

    def execute
      @result = false
      ActiveRecord::Base.transaction do
        rating = Rating.find_or_initialize_by(
          movie_id: movie_id,
          user_id: user.id
        )

        rating.rate = rate

        if rating.save
          @result = Movie.find(movie_id)
        else
          error!(rating.errors)
        end
      end
    end

    def error!(errors)
      merge_errors(errors)
      raise ActiveRecord::Rollback
    end
  end
end
