module Movies
  class Filter < ApplicationService
    include ::ValidateCategories

    ERROR_RATING_OUT_OF_RANGE = 'should be an integer from 1 to 5'.freeze

    def initialize(params)
      params = HashWithIndifferentAccess.new(params)
      @ratings = Array.wrap(params[:ratings]).map(&:to_i)
      @categories = Array.wrap(params[:categories]).map(&:to_i)
      @search = params[:search]
    end

    private

    attr_reader :ratings, :categories, :search

    def execute
      scope = Movie.all

      ratings.each_with_index do |r, i|
        if i == 0
          scope = scope.by_rate(r)
        else
          scope = scope.or(Movie.by_rate(r))
        end
      end

      scope = scope.by_category(categories) if categories.present?
      scope = scope.search(search) if search.present?
      scope
    end

    def validate!
      validate_ratings!
      validate_categories!
    end

    def validate_ratings!
      errors[:ratings] << ERROR_RATING_OUT_OF_RANGE if has_out_of_range_rating?
    end

    def has_out_of_range_rating?
      ratings.any? { |r| (1..5).exclude?(r) }
    end
  end
end
