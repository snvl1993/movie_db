module ValidateCategories
  extend ActiveSupport::Concern

  def validate_categories!
    invalid = invalid_categories
    errors[:categories] << invalid_categories_message(invalid) unless invalid.empty?
  end

  def invalid_categories
    records = Category.where(id: categories).pluck(:id).group_by(&:itself)
    categories.reject { |c| records[c] }
  end

  def invalid_categories_message(invalid)
    if invalid.size == 1
      "could not find category with ID = #{invalid.first}"
    else
      "could not find categories with IDs: #{invalid.join(', ')}"
    end
  end
end