class Rating < ApplicationRecord
  belongs_to :movie
  belongs_to :user

  validates :rate, presence: true, inclusion: { in: 1..5, message: 'should be from 1 to 5' }
  validates :movie_id, uniqueness: {
    scope: :user_id,
    message: 'user can only rate the movie once'
  }

  after_commit :recalculate_average!

  def recalculate_average!
    value = Rating.where(movie_id: movie_id).average(:rate)
    average = AverageRating.find_or_initialize_by(movie_id: movie_id)
    average.assign_attributes(value: value)
    average.save
  end
end
