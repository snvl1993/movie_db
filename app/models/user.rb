class User < ApplicationRecord
  acts_as_authentic do |c|
    c.crypto_provider = Authlogic::CryptoProviders::BCrypt
  end

  has_many :ratings
  has_many :movies

  validates :email,
    format: { with: /@/, message: 'should be a valid email address' },
    length: { maximum: 100 },
    uniqueness: { case_sensitive: false, if: :will_save_change_to_email? }

  validates :password,
    confirmation: { if: :require_password?, message: "doesn't match password" },
    length: { minimum: 8, if: :require_password? }

  validates :password_confirmation,
    length: { minimum: 8, if: :require_password? }

  def approve
    self.active = true
    self.approved = true
    self.confirmed = true
  end

  def approve!
    approve
    save
  end
end
