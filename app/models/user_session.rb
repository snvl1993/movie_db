class UserSession < Authlogic::Session::Base
  self.secure = !Rails.env.development?
end
