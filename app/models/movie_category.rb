class MovieCategory < ApplicationRecord
  belongs_to :movie
  belongs_to :category

  validates :movie_id, uniqueness: {
      scope: :category_id,
      message: 'should not have the same category twice'
  }
end
