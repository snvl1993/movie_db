class Movie < ApplicationRecord
  include PgSearch::Model

  paginates_per 5
  pg_search_scope :search,
    against: [[:title, 'A'], :text],
    using: { tsearch: { prefix: true } }

  belongs_to :user

  has_one :average_rating, dependent: :destroy
  has_many :ratings, dependent: :destroy
  has_many :movie_categories, dependent: :destroy
  has_many :categories, through: :movie_categories

  validates :title, presence: true, uniqueness: true

  scope :by_category, -> (category) do
    joins(:movie_categories)
      .where(movie_categories: { category_id: category })
  end

  scope :by_rate, -> (rate) do
    floor = rate.floor
    ceil = floor + 1

    joins(:average_rating)
      .where('average_ratings.value >= ? AND average_ratings.value < ?', floor, ceil)
  end

  def self.filter_by(params)
    service = Movies::Filter.call(params)
    service.result
  end
end
